# SimBridgeVRChat
SimbridgeVRChat is a reboot of the original Simbridge VRTK project to produce a smaller streaming-friendly metaverse world for Noisebridgers to use as a planning, exhibition and meetup collab tool.

* Unity project version: 2019.4.31 LTS compatible with VRChat
* https://docs.vrchat.com/docs/setting-up-the-sdk
* GitLFS is needed to download all the large binary art files.
* SourceTree Git Repo client is recommended and includes LFS if you don't have a favorite. https://www.sourcetreeapp.com
* You'll need to create a VRChat account through https://vrchat.com (This is recommended rather than using your steam ID).
* You'll need to spend time in VRChat gaining trust by socializing, exploring, making friends, saving avatars and favorite places, etc. before you get your VRChat account approved for developer access.
* You can run test builds on your local machine without pushing new builds to VRChat.
* If you put up new experimental versions, give them a suffix like "SimbridgeTest_____" where _____ is the title of your test build.
* You can put up private builds to test online but only you can invite people to those.
* If you want a version you upload to be searchable, you can choose Community Labs (people must enable Show Community Labs in their settings to find it). You only get 1 Community Labs upload per week but you can make multiple updates to it.

## RECOMMENDED VRCHAT SETTINGS
* Show Community Labs On (To find Simbridge in search)
* Personal Space Off (Unless you get harrassed)
* Skip go button in load On (saves time switching worlds)

## TROUBLESHOOTING
* If you get compile errors when you first load the project into Unity:

### UNSOLVED

Assets\VRCSDK\Dependencies\VRChat\Editor\ShaderStripping\StripAndroidAvatars.cs(7,26): error CS0234: The type or namespace name 'BuildPipeline' does not exist in the namespace 'VRC.SDKBase.Editor' (are you missing an assembly reference?)

